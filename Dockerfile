FROM eclipse-temurin:17-jre-alpine AS backend
WORKDIR /code
COPY paper.jar .
RUN mkdir plugins && mkdir plugins/dynmap
COPY dynmap.jar ./plugins
COPY configuration.txt ./plugins/dynmap
COPY eula.txt .
COPY server.properties .
EXPOSE 8123
CMD java -jar -Xms${XMS} -Xmx${XMX} -XX:+UseG1GC paper.jar
